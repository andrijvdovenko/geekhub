package com.geekhub.lessons.lesson7.storage;

import com.geekhub.lessons.lesson7.objects.Entity;
import com.geekhub.lessons.lesson7.objects.Ignore;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;

/**
 * Implementation of {@link com.geekhub.lessons.lesson7.storage.Storage} that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of {@link com.geekhub.lessons.lesson7.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName() + " WHERE id = " + id;
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        String sql = "SELECT * FROM " + clazz.getSimpleName();
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql));
            return result;
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        String sql = "DELETE FROM " + entity.getClass().getSimpleName() + " WHERE id = " + entity.getId();
        try (Statement statement = connection.createStatement()) {
            return statement.execute(sql);
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);
        String sql;
        StringBuilder sqlBuilder = new StringBuilder();
        if (entity.isNew()) {
            StringBuilder values = new StringBuilder(" VALUES (");
            String[] properties = new String[data.size()];
            data.keySet().toArray(properties);
            sqlBuilder.append("INSERT INTO " + entity.getClass().getSimpleName() + " (");
            for (int i = 0; i < properties.length; i++) {
                sqlBuilder.append(properties[i]);
                if (data.get(properties[i]) instanceof String) {
                    values.append("'" + data.get(properties[i]) + "'");
                } else {
                    values.append(data.get(properties[i]));
                }
                if (i < (properties.length - 1)) {
                    sqlBuilder.append(", ");
                    values.append(", ");
                }
            }
            sqlBuilder.append(")" + values.append(")"));
        } else {
            String[] properties = new String[data.size()];
            data.keySet().toArray(properties);
            sqlBuilder.append("UPDATE " + entity.getClass().getSimpleName() + " SET ");
            for (int i = 0; i < properties.length; i++) {
                sqlBuilder.append(properties[i] + " = ");
                if (data.get(properties[i]) instanceof String) {
                    sqlBuilder.append("'" + data.get(properties[i]) + "'");
                } else {
                    sqlBuilder.append(data.get(properties[i]));
                }
                if (i < (properties.length - 1)) {
                    sqlBuilder.append(", ");
                }
            }
            sqlBuilder.append(" WHERE id = " + entity.getId());
        }
        sql = sqlBuilder.toString();
        try (Statement statement = connection.createStatement()) {
            if (entity.isNew()) {
                statement.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
                ResultSet result = statement.getGeneratedKeys();
                result.first();
                entity.setId(result.getInt(1));
            } else {
                statement.executeUpdate(sql);
            }
        }
    }

    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        Map<String, Object> stringObjectMap = new HashMap<>();
        Field[] fields = entity.getClass().getDeclaredFields();
        for (Field field : fields) {
            if (!field.isAnnotationPresent(Ignore.class)) {
                Method getter = new PropertyDescriptor(field.getName(), entity.getClass()).getReadMethod();
                stringObjectMap.put(field.getName(), getter.invoke(entity));
            }
        }
        return stringObjectMap;
    }

    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        List<T> tList = new ArrayList<>();
        while (resultset.next()) {
            T t = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (!field.isAnnotationPresent(Ignore.class)) {
                    String fieldName = field.getName();
                    Object fieldValue = resultset.getObject(fieldName);
                    Method setter = new PropertyDescriptor(fieldName, clazz).getWriteMethod();
                    setter.invoke(t, fieldValue);
                }
            }
            t.setId(resultset.getInt("id"));
            tList.add(t);
        }
        return tList;
    }
}