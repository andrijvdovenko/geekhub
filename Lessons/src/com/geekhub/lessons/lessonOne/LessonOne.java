package com.geekhub.lessons.lessonOne;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 12.10.13
 * Time: 23:08
 * To change this template use File | Settings | File Templates.
 */

public class LessonOne {

    //function to read number from console
    public static int readConsoleNumber() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String s = bufferRead.readLine();
        return Integer.parseInt(s);
    }

    /*fuctorial function using simply cycle method*/
    private static int factorialFunction(int x) {
        if (x < 0)
            throw new IllegalArgumentException("The input number should be equal or higher then zero");
        int fact = 1;
        for (int i = 2; i <= x; i++)
            fact = fact * i;
        return fact;
    }

    /*fuctorial function using recursive method*/
    private static int factorialRecursion(int x) {
        if (x < 0) {
            throw new IllegalArgumentException("The input number should be equal or higher then zero");
        }
        if (x <= 1) {
            return 1;
        } else {
            return x * factorialRecursion(x - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println("to calculate Fuctorial press 1\n" +
                "to calculate FibonachiSequense press 2\n" +
                "to calculate FractionSequence press 3\n" +
                "to calculate CharSequence press 4");
        try {
            int n = readConsoleNumber();
            switch (n) {

                /*This block calculates Fuctorial*/
                case 1:
                    System.out.println("Fuctorial\n" +
                            "Input your number");
                    int fact = readConsoleNumber();
                        /* the call of fuctorial function using simply cycle method*/
                    int q = factorialFunction(fact);
                    System.out.println("Fuctorial using simple cycle = " + q);
                        /* the call of fuctorial function using recursive method */
                    int w = factorialRecursion(fact);
                    System.out.println("Fuctorial using recursive cycle = " + w);
                    break;

                /*this block calculates FibonachiSequense*/
                case 2:
                    System.out.println("FibonachiSequense\n" +
                            "Input your number");
                    int fib = readConsoleNumber();
                    int a = 1;
                    int b = 0;
                    for (int counter = 0; counter < fib; counter++) {
                        int fibo = a + b;
                        a = b;
                        b = fibo;
                        System.out.print(fibo + " ");
                    }

                    break;

                /*this block calculates FractionSequence*/
                case 3:
                    System.out.println("FractionSequence 1/n\n" +
                            "Input your number");
                    int frac = readConsoleNumber();
                    for (int counter = 1; counter <= frac; counter++) {
                        double e = 1.0 / counter;
                        System.out.print(e + " ");
                    }
                    break;

                  /*this block calculates CharSequence*/
                case 4:
                    System.out.println("CharSequence from m to n\n" +
                            "Input your number m");
                    int m = readConsoleNumber();
                    System.out.println("Input your number n");
                    BufferedReader bufferRead5 = new BufferedReader(new InputStreamReader(System.in));
                    String chn = bufferRead5.readLine();
                    int nn = Integer.parseInt(chn);
                    for (int counter = m; counter <= nn; counter++) {
                        System.out.print((char) counter + " ");
                    }
                    break;
                default:
                    System.out.println("Error! This is incorrect input number!");
                    break;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


