package com.geekhub.lessons.lessonThree.taskOne;

import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 30.10.13
 * Time: 1:54
 * To change this template use File | Settings | File Templates.
 */
public class ComparableMain {
    public static void main(String[] args) {
        CompMan[] man = new CompMan[6];
        man[0] = new CompMan("Ivan", 50);
        man[1] = new CompMan("Nick", 150);
        man[2] = new CompMan("Dimon", 80);
        man[3] = new CompMan("Stas", 200);
        man[4] = new CompMan("Sergij", 20);
        man[5] = new CompMan("Anatoly", 75);

        CompCar[] car = new CompCar[7];
        car[0] = new CompCar("Ferrari", 300, "Ivan");
        car[1] = new CompCar("Mazeratti", 250, "Ivan");
        car[2] = new CompCar("Smart", 80, "Ivan");
        car[3] = new CompCar("Tavria", 200, "Andrij");
        car[4] = new CompCar("Jiguli", 70, "Nick");
        car[5] = new CompCar("Lamborgini", 350, "Sam");
        car[6] = new CompCar("Lancia", 400, "Fedor");

        System.out.println("\nman array as it is\n");
        for (CompMan e : man) {
            System.out.println("Name: " + e.name + " IQ: " + e.iq);
        }

        //making copy of arrays
        CompMan[] mans = new CompMan[6];
        System.arraycopy(man, 0, mans, 0, man.length);
        CompCar[] cars = new CompCar[7];
        System.arraycopy(car, 0, cars, 0, car.length);

        //Sort of array mans
        Arrays.sort(mans);
        System.out.println("\nsorted man array by name\n");
        for (CompMan e : mans) {
            System.out.println("Name: " + e.name + " IQ: " + e.iq);
        }

        System.out.println("\ncar array as it is\n");
        for (CompCar c : car) {
            System.out.println("Model: " + c.model + " TopSpeed: " + c.topSpeed + " Owner: " + c.owner);
        }
        System.out.println("\ncar array sorted by owner\n");

        //Sort of array mans
        Arrays.sort(cars);
        for (CompCar c : cars) {
            System.out.println("Model: " + c.model + " TopSpeed: " + c.topSpeed + " Owner: " + c.owner);
        }
    }
}
