package com.geekhub.lessons.lessonThree.taskOne;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 05.11.13
 * Time: 20:10
 * To change this template use File | Settings | File Templates.
 */
public class CompCar implements Comparable {
    String model;
    int topSpeed;
    String owner;

    CompCar(String model, int topSpeed, String owner) {
        this.model = model;
        this.topSpeed = topSpeed;
        this.owner = owner;
    }
    @Override
    public int compareTo(Object obj) {
        CompCar entry = (CompCar) obj;

        int result = owner.compareTo(entry.owner);
        if(result != 0) {
            if (result<0){
                return -1;
            } else
                return 1;
        }
        return 0;

    }

}
