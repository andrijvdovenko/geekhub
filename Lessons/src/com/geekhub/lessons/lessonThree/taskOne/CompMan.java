package com.geekhub.lessons.lessonThree.taskOne;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 30.10.13
 * Time: 1:32
 * To change this template use File | Settings | File Templates.
 */
public class CompMan implements Comparable {
    String name;
    int iq;

    CompMan(String name, int iq) {
        this.name = name;
        this.iq = iq;
    }
    @Override
    public int compareTo(Object obj) {
        CompMan entry = (CompMan) obj;

        int result = name.compareTo(entry.name);
        if(result != 0) {
            if (result<0){
                return -1;
            } else
                  return 1;
        }
        return 0;
        /*result = iq - entry.iq;
        if(result != 0) {
            return (int) result / Math.abs( result );
        }
        return 0;
                 */
    }

}
