package com.geekhub.lessons.lessonThree.taskThree;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 16.10.13
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */
abstract class Vehicle implements Driveable {

    private double currentSpeed;        //curent speed
    private double topSpeed;            //top speed
    private double gasTank;             //gas tank amount
    private double gas;                 //gas in the tank
    private double gasconsumption;  //gas consumprion l/100km


    public double getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(double newCurrentSpeed) {
        currentSpeed = newCurrentSpeed;
    }

    public double getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(double newSpeed) {
        topSpeed = newSpeed;
    }

    public double getGasTank() {
        return gasTank;
    }

    public void setGasTank(double newGasTank) {
        gasTank = newGasTank;
    }

    public double getGas() {
        return gas;
    }

    public void setGas(double newGas) {
        gas = newGas;
    }

    public double getGasconsumption() {
        return gasconsumption;
    }

    public void setGasconsumption(double newGascons) {
        gasconsumption = newGascons;
    }

    /*Implementation of basic Vechicle class that is abstract & have 3 methods*/

    public abstract void accelerate(double a) throws OverSpeedExeption;


    public void brake(double a) throws LowSpeedException {
        if (this.currentSpeed >= a) {
            System.out.println("My currentspeed is: " + this.getCurrentSpeed());
            System.out.println("Braking on the value:" + a);
            this.setCurrentSpeed(getCurrentSpeed() - a);
            System.out.println("My current speed after breaking is: " + this.getCurrentSpeed());
        } else {
            throw new LowSpeedException("Current speed is too low");
        }
    }


    public void turn(double a) {
        System.out.println("Turning... " + a + " degree");
    }

    public void ride(double a) throws NoFuelException, LowSpeedException {
        System.out.println("Current amount of gas: " + this.getGas() + " l");
        System.out.println("Current speed: " + this.getCurrentSpeed()+ "km/h");
        System.out.println("Time to ride is: " + a / this.getCurrentSpeed() + " hours");
        System.out.println("We need: " + (a / this.getCurrentSpeed()) * this.getGasconsumption() + " l of gas");
        if (((a / this.getCurrentSpeed()) * this.getGasconsumption()) <= this.getGas()) {
            this.setGas(this.getGas() - ((a / this.getCurrentSpeed()) * this.getGasconsumption()));
            System.out.println("Amount of gas after trip is : " + this.getGas());
        } else if (this.currentSpeed == 0){
            throw new LowSpeedException("Current speed is 0 km/hours");
        } else {
            throw new NoFuelException("There is no fuel!");
        }

    }
}
