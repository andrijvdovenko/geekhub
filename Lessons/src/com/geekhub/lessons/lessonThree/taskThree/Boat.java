package com.geekhub.lessons.lessonThree.taskThree;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 20.10.13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
public class Boat extends Vehicle {


    public void accelerate(double a) throws OverSpeedExeption {
        if ((getCurrentSpeed() + a) <= getTopSpeed()) {
            System.out.println("My currentspeed is: " + this.getCurrentSpeed());
            System.out.println("Acceleraring on the value:" + a);
            this.setCurrentSpeed(this.getCurrentSpeed() + a);
            System.out.println("My current speed after accelerating is: " + this.getCurrentSpeed());
        } else {
            throw new OverSpeedExeption("We reached top speed!");
        }
        GasolineEngine myEngine = new GasolineEngine();
        myEngine.enginePower(getGas());
        System.out.println("Engine is working");
        Propeller myProp = new Propeller();
        myProp.force(getCurrentSpeed());
    }
}


