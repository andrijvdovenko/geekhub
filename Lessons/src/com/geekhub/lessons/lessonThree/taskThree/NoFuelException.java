package com.geekhub.lessons.lessonThree.taskThree;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 10.11.13
 * Time: 23:09
 * To change this template use File | Settings | File Templates.
 */
public class NoFuelException extends Exception {
    public NoFuelException(String message) {
        super(message);
    }
}
