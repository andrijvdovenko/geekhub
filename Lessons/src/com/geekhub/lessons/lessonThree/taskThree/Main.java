package com.geekhub.lessons.lessonThree.taskThree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 20.10.13
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public class Main {

    public static int cycle;

    public static int readConsoleNumber() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String s = bufferRead.readLine();
        return Integer.parseInt(s);
    }

    /*All class hierarchy in use */

    public static void main(String[] args) {

        do {
            System.out.println("To drive a solar car press --------- '1'\n" +
                    "To drive a boat press -------------- '2'\n" +
                    "To drive a gasoline car car press -- '3'\n" +
                    "To exit press ---------------------- '4'");
            try {
                int vechicle = readConsoleNumber();
                System.out.println("You choise is: '" + vechicle + "' ");
                switch (vechicle) {
                    case 1:     /*Solar car actions*/
                        Vehicle sunny = new SolarCar();
                        System.out.println("Your SolarCar have been created");
                        System.out.println("Input amount of gasTank");
                        sunny.setGasTank(readConsoleNumber());
                        System.out.println("Input SolarCar top speed");
                        sunny.setTopSpeed(readConsoleNumber());
                        System.out.println("Input start amount of gas");
                        sunny.setGas(readConsoleNumber());
                        System.out.println("Input gasConsumption");
                        sunny.setGasconsumption(readConsoleNumber());
                        do {
                            System.out.println("You can:\n" +
                                    "To accelerate press ----- 1\n" +
                                    "To brake press ---------- 2\n" +
                                    "To turn press  ---------- 3\n" +
                                    "To exit SolarCar -------- 4\n");
                            try {
                                int boat = readConsoleNumber();
                                switch (boat) {
                                    case 1:
                                        System.out.println("Input value to accelerate");
                                        sunny.accelerate(readConsoleNumber());
                                        break;
                                    case 2:
                                        System.out.println("Input value to break");
                                        sunny.brake(readConsoleNumber());
                                        break;
                                    case 3:
                                        System.out.println("Input value to turn");
                                        sunny.turn(readConsoleNumber());
                                        break;
                                    case 4:
                                        cycle = 4;
                                }

                            } catch (LowSpeedException l) {
                                System.out.println("I can't brake anymore! " + l.getMessage());
                            } catch (OverSpeedExeption o) {
                                System.out.println("I cannot accelerate anymore! " + o.getMessage());
                            } catch (IOException e) {
                                System.out.println("Error! This is incorrect input number!");
                            }
                        }
                        while (cycle != 4);
                        break;
                    case 2:        /*Boat actions*/
                        Vehicle myBoat = new Boat();  //gas tank, current speed, top speed
                        System.out.println("Your Boat have been created");
                        System.out.println("Input amount of gasTank");
                        myBoat.setGasTank(readConsoleNumber());
                        System.out.println("Input boats top speed");
                        myBoat.setTopSpeed(readConsoleNumber());
                        System.out.println("Input start amount of gas");
                        myBoat.setGas(readConsoleNumber());
                        System.out.println("Input gasConsumption");
                        myBoat.setGasconsumption(readConsoleNumber());
                        do {
                            System.out.println("You can:\n" +
                                    "To accelerate press -------------- 1\n" +
                                    "To brake press ------------------- 2\n" +
                                    "To turn press -------------------- 3\n" +
                                    "To ride 100 km press ------------- 4\n" +
                                    "To fill 10 l of gas press--------- 5\n" +
                                    "To accelerate 10 km/h press ------ 6\n" +
                                    "To break 10 km/h press ----------- 7\n" +
                                    "To show amount of gas press ------ 8\n" +
                                    "To exit Boat --------------------- 9\n");
                            try {
                                int boat = readConsoleNumber();
                                switch (boat) {
                                    case 1:
                                        System.out.println("Input value to accelerate");
                                        myBoat.accelerate(readConsoleNumber());
                                        break;
                                    case 2:
                                        System.out.println("Input value to break");
                                        myBoat.brake(readConsoleNumber());
                                        break;
                                    case 3:
                                        System.out.println("Input value to turn");
                                        myBoat.turn(readConsoleNumber());
                                        break;
                                    case 4:
                                        System.out.println("Riding 100 km");
                                        myBoat.ride(100);
                                        break;
                                    case 5:
                                        if ((myBoat.getGas() + 10) <= myBoat.getGasTank()) {
                                            myBoat.setGas(myBoat.getGas() + 10);
                                            System.out.println("Amount of gas after filling 10 l is " + myBoat.getGas());
                                        } else {
                                            throw new GasOverflowExeption("I cant fill gas anymore!");
                                        }
                                        break;
                                    case 6:
                                        myBoat.accelerate(10);
                                        System.out.println("Current speed after acceleration 10 km/h is " + myBoat.getCurrentSpeed());
                                        break;
                                    case 7:
                                        myBoat.brake(10);
                                        System.out.println("Current speed after breaking 10 km/h is " + myBoat.getCurrentSpeed());
                                        break;
                                    case 8:
                                        System.out.println(myBoat.getGas());
                                        break;
                                    case 9:
                                        cycle = 5;
                                }

                            } catch (GasOverflowExeption g) {
                                System.out.println("There is no free space in GasTank! " + g.getMessage());
                            } catch (LowSpeedException l) {
                                System.out.println("Attention! " + l.getMessage());
                            } catch (NoFuelException n) {
                                System.out.println("We can't drive anymore! " + n.getMessage());
                            } catch (OverSpeedExeption o) {
                                System.out.println("I cannot accelerate anymore! " + o.getMessage());
                            } catch (IOException e) {
                                System.out.println("Error! This is incorrect input number!");
                            }
                        }
                        while (cycle != 5);
                        break;
                    case 3:          /*Gasoline car actions*/
                        Vehicle gCar = new GasolineCar();
                        System.out.println("Your GasolineCar have been created");
                        System.out.println("Input amount of gasTank");
                        gCar.setGasTank(readConsoleNumber());
                        System.out.println("Input GasolineCar top speed");
                        gCar.setTopSpeed(readConsoleNumber());
                        System.out.println("Input start amount of gas");
                        gCar.setGas(readConsoleNumber());
                        System.out.println("Input gasConsumption");
                        gCar.setGasconsumption(readConsoleNumber());
                        do {
                            System.out.println("You can:\n" +
                                    "To accelerate press -------------- 1\n" +
                                    "To brake press ------------------- 2\n" +
                                    "To turn press  ------------------- 3\n" +
                                    "To ride 100 km press ------------- 4\n" +
                                    "To fill 10 l of gas press -------- 5\n" +
                                    "To accelerate 10 km/h press ------ 6\n" +
                                    "To break 10 km/h press ----------- 7\n" +
                                    "To show amount of gas press ------ 8\n" +
                                    "To exit GasolineCar -------------- 9\n");
                            try {
                                int boat = readConsoleNumber();
                                switch (boat) {
                                    case 1:
                                        System.out.println("Input value to accelerate");
                                        gCar.accelerate(readConsoleNumber());
                                        break;
                                    case 2:
                                        System.out.println("Input value to break");
                                        gCar.brake(readConsoleNumber());
                                        break;
                                    case 3:
                                        System.out.println("Input value to turn");
                                        gCar.turn(readConsoleNumber());
                                        break;
                                    case 4:
                                        System.out.println("Riding 100 km");
                                        gCar.ride(100);
                                        break;
                                    case 5:
                                        if ((gCar.getGas() + 10) <= gCar.getGasTank()) {
                                            gCar.setGas(gCar.getGas() + 10);
                                            System.out.println("Amount of gas after filling 10 l is " + gCar.getGas());
                                        } else {
                                            throw new GasOverflowExeption("I cant fill gas anymore!");
                                        }
                                        break;
                                    case 6:
                                        gCar.accelerate(10);
                                        System.out.println("Current speed after acceleration 10 km/h is " + gCar.getCurrentSpeed());
                                        break;
                                    case 7:
                                        gCar.brake(10);
                                        System.out.println("Current speed after breaking 10 km/h is " + gCar.getCurrentSpeed());
                                        break;
                                    case 8:
                                        System.out.println(gCar.getGas());
                                        break;
                                    case 9:
                                        cycle = 6;
                                }

                            } catch (GasOverflowExeption g) {
                                System.out.println("There is no free space in GasTank! " + g.getMessage());
                            } catch (LowSpeedException l) {
                                System.out.println("Attention! " + l.getMessage());
                            } catch (NoFuelException n) {
                                System.out.println("We can't drive anymore! " + n.getMessage());
                            } catch (OverSpeedExeption o) {
                                System.out.println("I cannot accelerate anymore! " + o.getMessage());
                            } catch (IOException e) {
                                System.out.println("Error! This is incorrect input number!");
                            }
                        }
                        while (cycle != 6);
                        break;
                    case 4:
                        cycle = 7;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (cycle != 7);
    }
}
