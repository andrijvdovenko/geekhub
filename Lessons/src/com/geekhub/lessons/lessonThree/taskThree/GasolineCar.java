package com.geekhub.lessons.lessonThree.taskThree;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 21.10.13
 * Time: 1:35
 * To change this template use File | Settings | File Templates.
 */
public class GasolineCar extends Vehicle {


    /*class that describes gasoline car*/

    public void accelerate(double a) throws OverSpeedExeption {
        if ((getCurrentSpeed() + a) <= getTopSpeed()) {
            System.out.println("My currentspeed is: " + this.getCurrentSpeed());
            System.out.println("Acceleraring on the value:" + a);
            this.setCurrentSpeed(this.getCurrentSpeed() + a);
            System.out.println("My current speed after accelerating is: " + this.getCurrentSpeed());
        } else {
            throw new OverSpeedExeption("We reached top speed!");
        }
        GasolineEngine myEngine = new GasolineEngine();
        myEngine.enginePower(this.getGas());
        System.out.println("Engine is working");
        Wheel myWheel = new Wheel();
        myWheel.force(this.getCurrentSpeed());
    }


}
