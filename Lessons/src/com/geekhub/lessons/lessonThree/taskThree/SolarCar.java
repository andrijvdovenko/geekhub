package com.geekhub.lessons.lessonThree.taskThree;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 17.10.13
 * Time: 2:27
 * To change this template use File | Settings | File Templates.
 */

/*Implementation of Solar-powered car class*/
public class SolarCar extends Vehicle {
    /*Switching on The Solar-powered car*/


    public void accelerate(double a) throws OverSpeedExeption {
        if ((getCurrentSpeed() + a) <= getTopSpeed()) {
            System.out.println("My currentspeed is: " + this.getCurrentSpeed());
            System.out.println("Acceleraring on the value:" + a);
            this.setCurrentSpeed(this.getCurrentSpeed() + a);
            System.out.println("My current speed after accelerating is: " + this.getCurrentSpeed());
        } else {

            throw new OverSpeedExeption("We reached top speed!");
        }
        ElectricEngine myEngine = new ElectricEngine();
        myEngine.enginePower(getGas());
        System.out.println("Engine is working");
        Wheel myWheel = new Wheel();
        myWheel.force(getCurrentSpeed());
    }

}
