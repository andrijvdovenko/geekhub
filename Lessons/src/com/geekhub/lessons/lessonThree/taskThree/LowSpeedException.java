package com.geekhub.lessons.lessonThree.taskThree;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 10.11.13
 * Time: 23:47
 * To change this template use File | Settings | File Templates.
 */
public class LowSpeedException extends Exception {
    public LowSpeedException(String message) {
        super(message);
    }
}
