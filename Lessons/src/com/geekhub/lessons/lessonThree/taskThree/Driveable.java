package com.geekhub.lessons.lessonThree.taskThree;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 17.10.13
 * Time: 2:17
 * To change this template use File | Settings | File Templates.
 */
public interface Driveable {
    /*description of Driveable intarface*/
    void accelerate(double a) throws OverSpeedExeption;

    void brake(double a) throws LowSpeedException;

    void turn(double a);
}
