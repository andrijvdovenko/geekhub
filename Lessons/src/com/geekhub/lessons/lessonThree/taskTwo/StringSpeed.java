package com.geekhub.lessons.lessonThree.taskTwo;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 29.10.13
 * Time: 21:26
 * To change this template use File | Settings | File Templates.
 */
public class StringSpeed {

    public static void main(String[] args) {
        System.out.println("Comparing speed of concatenation using StringBuilder, StringBuffer, String\n" +
                "Input number of iterations:");
        try {
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String s = bufferRead.readLine();
            int count = Integer.parseInt(s);
            String stringSample = "Sample";
            String someString = "SomeStrinIsBigEnoughOrNotMayBeIwonderIfItRatherSmallWhatShouldIDoIWonder";

            //The StringBuilder concatenation
            long a = System.currentTimeMillis();
            StringBuilder stringBuilderSample = new StringBuilder();
            stringBuilderSample.append("BuilderSample");
            for (int i = 0; i < count; i++) {
                stringBuilderSample.append(someString);
            }
            long b = System.currentTimeMillis();
            System.out.print("The concatenation time using StringBuilder is: ");
            System.out.print(b - a);
            System.out.println(" ms");

            //The StringBuffer concatenation
            a = System.currentTimeMillis();
            StringBuffer stringBufferSample = new StringBuffer();
            stringBufferSample.append("BufferSample");
            for (int i = 0; i < count; i++) {
                stringBufferSample.append(someString);
            }
            b = System.currentTimeMillis();
            System.out.print("The concatenation time using StringBuffer is: ");
            System.out.print(b - a);
            System.out.println(" ms");

            //The String concatenation
            a = System.currentTimeMillis();
            for (int i = 0; i < count; i++) {
                stringSample += someString;
            }
            b = System.currentTimeMillis();
            System.out.print("The concatenation time using String is: ");
            System.out.print(b - a);
            System.out.println(" ms");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
