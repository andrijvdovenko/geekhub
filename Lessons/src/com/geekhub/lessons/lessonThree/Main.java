package com.geekhub.lessons.lessonThree;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 20.10.13
 * Time: 13:55
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    /*All class hierarchy in use */
    public static void main(String[] args) {

        System.out.println("To drive a solarcar press --- '1'\n" +
                "To drive a boat press ------- '2'\n" +
                "To drive a solar car press -- '3'\n" +
                "To exit press --------------- '4'");
        WrongInputException wrIn = new WrongInputException("Please input correct numbers!");
        try {
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String choise = bufferRead.readLine();
            int vechicle = Integer.parseInt(choise);
            System.out.println("You choise is: '" + vechicle + "' ");
            switch (vechicle) {
                case 1:     /*Solar car actions*/
                    System.out.println("--------SolarCar--------");
                    Vehicle sunny = new SolarCar();
                    sunny.accelerate();
                    sunny.turn("left");
                    sunny.brake();
                    break;
                case 2:        /*Boat actions*/
                    System.out.println("--------Boat--------");
                    Vehicle myBoat = new Boat();
                    myBoat.accelerate();
                    myBoat.turn("right");
                    myBoat.brake();
                    break;
                case 3:          /*Gasoline car actions*/
                    System.out.println("--------GasolineCar--------");
                    Vehicle gCar = new GasolineCar();
                    gCar.accelerate();
                    gCar.turn("back");
                    gCar.brake();
                    break;
                case 4:
                    break;
                default:
                    throw wrIn;
            }
        } catch (WrongInputException i) {
            System.out.println("Input choise Error occured! This choise is out of range! "+i.getMessage());
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
