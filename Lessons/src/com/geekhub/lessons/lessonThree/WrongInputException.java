package com.geekhub.lessons.lessonThree;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 05.11.13
 * Time: 21:25
 * To change this template use File | Settings | File Templates.
 */
public class WrongInputException extends Exception {
    public WrongInputException(String message) {
        super(message);
    }
}

