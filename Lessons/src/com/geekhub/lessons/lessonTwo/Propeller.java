package com.geekhub.lessons.lessonTwo;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 20.10.13
 * Time: 13:50
 * To change this template use File | Settings | File Templates.
 */
public class Propeller implements MotionProvider {

    /*Implementation of propeller class that mooves boat*/
    public void force(double x) {
        System.out.println("Mooving my Propeller... Average speed is " + x + "km/h");
    }
}
