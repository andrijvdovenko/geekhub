package com.geekhub.lessons.lessonTwo;

import sun.org.mozilla.javascript.internal.ast.IfStatement;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 16.10.13
 * Time: 22:29
 * To change this template use File | Settings | File Templates.
 */
abstract class Vehicle implements Driveable {

    private double currentSpeed;        //curent speed
    private double topSpeed;            //top speed
    private double gasTank;             //gas tank amount
    private double gas;                 //gas in the tank
    private double gasconsumption;  //gas consumprion l/100km


    public double getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(double newCurrentSpeed) {
        currentSpeed = newCurrentSpeed;
    }

    public double getTopSpeed() {
        return topSpeed;
    }

    public void setTopSpeed(double newSpeed) {
        topSpeed = newSpeed;
    }

    public double getGasTank() {
        return gasTank;
    }

    public void setGasTank(double newGasTank) {
        gasTank = newGasTank;
    }

    public double getGas() {
        return gas;
    }

    public void setGas(double newGas) {
        gas = newGas;
    }

    public double getGasconsumption() {
        return gasconsumption;
    }

    public void setGasconsumption(double newGascons) {
        gasconsumption = newGascons;
    }

    /*Implementation of basic Vechicle class that is abstract & have 3 methods*/

    public void accelerate(double a) {
        System.out.println("My currentspeed is: " + this.getCurrentSpeed());
        System.out.println("Acceleraring on the value:" + a);
        this.setCurrentSpeed(this.getCurrentSpeed() + a);
        System.out.println("My current speed after accelerating is: " + this.getCurrentSpeed());
    }


    public void brake(double a) {
        System.out.println("My currentspeed is: " + this.getCurrentSpeed());
        System.out.println("Braking on the value:" + a);
        this.setCurrentSpeed(getCurrentSpeed() - a);
        System.out.println("My current speed after breaking is: " + this.getCurrentSpeed());
    }


    public void turn(double a) {
        System.out.println("Turning... " + a + " degree");
    }

}
