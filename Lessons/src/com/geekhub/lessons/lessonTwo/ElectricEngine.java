package com.geekhub.lessons.lessonTwo;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 20.10.13
 * Time: 13:53
 * To change this template use File | Settings | File Templates.
 */
public class ElectricEngine implements ForceProvider {
    /*Clas that discribes Electric engine*/
    public void enginePower(double x) {
        System.out.println("Electric engine have been started!");
        SolarBattery myBat = new SolarBattery();
        myBat.fuel(x);
        System.out.println("We can drive till sun set down!");
    }

}
