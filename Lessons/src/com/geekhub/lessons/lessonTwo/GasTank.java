package com.geekhub.lessons.lessonTwo;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 20.10.13
 * Time: 13:41
 * To change this template use File | Settings | File Templates.
 */
public class GasTank implements EnergyCapture {
    /*class that implements Gas tank*/

    public void fuel(double x) {
        System.out.println("There are '" + x + "'" +
                "l gas in my tank");
    }
}
