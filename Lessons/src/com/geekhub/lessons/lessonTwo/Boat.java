package com.geekhub.lessons.lessonTwo;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 20.10.13
 * Time: 13:11
 * To change this template use File | Settings | File Templates.
 */
public class Boat extends Vehicle {


    public void accelerate(double a) {
        super.accelerate(a);
        GasolineEngine myEngine = new GasolineEngine();
        myEngine.enginePower(getGas());
        System.out.println("Engine is working");
        Propeller myProp = new Propeller();
        myProp.force(getCurrentSpeed());
    }


}
