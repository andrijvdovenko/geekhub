package com.geekhub.lessons.lessonTwo;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 21.10.13
 * Time: 1:35
 * To change this template use File | Settings | File Templates.
 */
public class GasolineCar extends Vehicle {


    /*class that describes gasoline car*/

    public void accelerate(double a){
        super.accelerate(a);
        GasolineEngine myEngine = new GasolineEngine();
        myEngine.enginePower(getGas());
        System.out.println("Engine is working");
        Wheel myWheel = new Wheel();
        myWheel.force(getCurrentSpeed());
    }
}
