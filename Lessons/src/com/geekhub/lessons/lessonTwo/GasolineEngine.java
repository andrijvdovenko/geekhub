package com.geekhub.lessons.lessonTwo;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 20.10.13
 * Time: 13:52
 * To change this template use File | Settings | File Templates.
 */
public class GasolineEngine implements ForceProvider {

    /*Class that implements gasolime engine*/
    public void enginePower(double x) {
        System.out.println("Engine have been started!");
        GasTank myGas = new GasTank();
        myGas.fuel(x);

    }
}
