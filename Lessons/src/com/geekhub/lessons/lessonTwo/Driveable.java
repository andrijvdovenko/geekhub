package com.geekhub.lessons.lessonTwo;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 17.10.13
 * Time: 2:17
 * To change this template use File | Settings | File Templates.
 */
public interface Driveable {
    /*description of Driveable intarface*/
    void accelerate(double a);

    void brake(double a);

    void turn(double a);
}
