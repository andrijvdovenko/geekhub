package com.geekhub.lessons.lessonTwo;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 17.10.13
 * Time: 2:27
 * To change this template use File | Settings | File Templates.
 */

/*Implementation of Solar-powered car class*/
public class SolarCar extends Vehicle {
    /*Switching on The Solar-powered car*/
    public void accelerate(double a) {
        super.accelerate(a);
        ElectricEngine myEngine = new ElectricEngine();
        myEngine.enginePower(getGas());
        System.out.println("Engine is working");
        Wheel myWheel = new Wheel();
        myWheel.force(getCurrentSpeed());
    }

}
