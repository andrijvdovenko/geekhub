package com.geekhub.lessons.lessonFour.taskOne;

//import java.util.HashSet;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 11.11.13
 * Time: 1:41
 * To change this template use File | Settings | File Templates.
 */
public class SetOperationsImpl implements SetOperations {
    public boolean equals(Set a, Set b) {
        System.out.println("Equals");
        Set<Integer> setA = new HashSet<Integer>(a);//set A
        Set<Integer> setB = new HashSet<Integer>(b);//set B
        System.out.println("Set A: ");//set output A
        System.out.println(setA);
       /* System.out.print("\n[");

        while(itset1.hasNext()) {
            System.out.print( itset1.next()+" ");
        }
        System.out.println("]"); */
        System.out.println("Set B: ");//set output B
        System.out.println(setB);

        if (setA.equals(setB)) {
            System.out.println("Yes it equals!");
            return true;
        } else {
            System.out.println("No it not equals!");
            return false;
        }
    }

    public Set union(Set a, Set b) {
        System.out.println("Union");
        Set<Integer> setA = new HashSet<Integer>(a);//set A
        Set<Integer> setB = new HashSet<Integer>(b);//set B
        //Iterator<Integer> itA =setA.iterator();
        //Iterator<Integer> itB =setB.iterator();

        System.out.println("Set A: ");//set output A
        System.out.println(setA);
       /* System.out.print("\n[");

        while(itset1.hasNext()) {
            System.out.print( itset1.next()+" ");
        }
        System.out.println("]"); */
        System.out.println("Set B: ");//set output B
        System.out.println(setB);
      /*  System.out.print("\n[");
        while(itset2.hasNext()) {
            System.out.print(itset2.next()+" ");
        }
        System.out.println("]");*/

        setA.addAll(setB);
        System.out.println("Union set: " + "\n" + setA);
        return setA;
    }

    public Set subtract(Set a, Set b) {
        System.out.println("Subtract");
        Set<Integer> setA = new HashSet<Integer>(a);//set A
        Set<Integer> setB = new HashSet<Integer>(b);//set B
        //Iterator<Integer> itA =setA.iterator();
        //Iterator<Integer> itB =setB.iterator();

        System.out.println("Set A: ");//output set A
        System.out.println(setA);
       /* System.out.print("\n[");

        while(itA.hasNext()) {
            System.out.print( itset1.next()+" ");
        }
        System.out.println("]"); */
        System.out.println("Set B: ");//output set B
        System.out.println(setB);
      /*  System.out.print("\n[");
        while(itB.hasNext()) {
            System.out.print(itset2.next()+" ");
        }
        System.out.println("]");*/

        setA.removeAll(setB);
        System.out.println("Subtract set: " + "\n" + setA);
        return setA;
    }


    public Set intersect(Set a, Set b) {
        System.out.println("Intersect");
        Set<Integer> setA = new HashSet<Integer>(a);//set A
        Set<Integer> setB = new HashSet<Integer>(b);//set B
        //Iterator<Integer> itA =setA.iterator();
        //Iterator<Integer> itB =setB.iterator();

        System.out.println("Set A: ");//set output A
        System.out.println(setA);
       /* System.out.print("\n[");

        while(itA.hasNext()) {
            System.out.print( itset1.next()+" ");
        }
        System.out.println("]"); */
        System.out.println("Set B: ");//set output B
        System.out.println(setB);
      /*  System.out.print("\n[");
        while(itB.hasNext()) {
            System.out.print(itset2.next()+" ");
        }
        System.out.println("]");*/

        setA.retainAll(setB);
        System.out.println("Intersected set: " + "\n" + setA);
        return setA;
    }

    public Set symmetricSubtract(Set a, Set b) {
        System.out.println("Symmetric Subtract");
        Set<Integer> setA = new HashSet<Integer>(subtract(a, b));//set A
        Set<Integer> setB = new HashSet<Integer>(subtract(b, a));//set B
        //Iterator<Integer> itA =setA.iterator();
        //Iterator<Integer> itB =setB.iterator();

        System.out.println("Set A: ");//output set A
        System.out.println(setA);
       /* System.out.print("\n[");

        while(itA.hasNext()) {
            System.out.print( itset1.next()+" ");
        }
        System.out.println("]"); */
        System.out.println("Set B: ");//output set B
        System.out.println(setB);
      /*  System.out.print("\n[");
        while(itB.hasNext()) {
            System.out.print(itset2.next()+" ");
        }
        System.out.println("]");*/

        setA.addAll(setB);
        System.out.println("Symmetric subtract set: " + "\n" + setA);
        return setA;
    }
}


