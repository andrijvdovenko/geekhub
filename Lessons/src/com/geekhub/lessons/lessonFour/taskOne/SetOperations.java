package com.geekhub.lessons.lessonFour.taskOne;

import java.util.Set;
/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 05.11.13
 * Time: 21:48
 * To change this template use File | Settings | File Templates.
 */
public interface SetOperations {
    public boolean equals(Set a, Set b);
    public Set union(Set a, Set b);
    public Set subtract(Set a, Set b);
    public Set intersect(Set a, Set b);
    public Set symmetricSubtract(Set a, Set b);

}
