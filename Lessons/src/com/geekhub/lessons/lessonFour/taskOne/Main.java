package com.geekhub.lessons.lessonFour.taskOne;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Андрій
 * Date: 13.11.13
 * Time: 0:18
 * To change this template use File | Settings | File Templates.
 */
public class Main {
    public static void main(String[] args) {

        SetOperations operations = new SetOperationsImpl();
        System.out.println("Set operations");

        HashSet a = new HashSet();

        a.add(1);
        a.add(3);
        a.add(5);
        a.add(7);
        a.add(9);
        a.add(11);

        HashSet b = new HashSet();

        b.add(2);
        b.add(4);
        b.add(5);
        b.add(8);
        b.add(9);

        HashSet c = new HashSet();

        c.add(1);
        c.add(3);
        c.add(5);
        c.add(7);
        c.add(9);
        c.add(11);

        //Union
        operations.equals(a, b);
        operations.equals(a, c);
        //Union
        operations.union(a, b);
        operations.union(a, c);
        operations.union(b, c);
        //Substruct
        operations.subtract(a, b);
        operations.subtract(a, c);
        operations.subtract(b, c);
        //Intersect
        operations.intersect(a, b);
        operations.intersect(a, c);
        operations.intersect(b, c);
        //SymmetricIntersect
        operations.symmetricSubtract(a, b);
        operations.symmetricSubtract(a, c);
        operations.symmetricSubtract(b, c);
    }
}
