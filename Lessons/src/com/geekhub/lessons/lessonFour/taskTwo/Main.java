package com.geekhub.lessons.lessonFour.taskTwo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
    public static int cycle;

    public static int readConsoleNumber() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        String s = bufferRead.readLine();
        return Integer.parseInt(s);
    }

    public static String readConsole() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        return bufferRead.readLine();
    }

    public static void main(String[] args) {
        int year, month, day;
        Date date;
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        GregorianCalendar calendar = new GregorianCalendar(2013, Calendar.OCTOBER, 26);
        TaskManager taskManager = new TaskManagerImpl();

        do {
            System.out.println("Task Manager. You can:");
            System.out.println("To add task enter ------------------ '1'");
            System.out.println("To remove task be date enter ------- '2'");
            System.out.println("To get categories of tasks enter --- '3'");
            System.out.println("To get tasks by categories enter --- '4'");
            System.out.println("To get task by category enter ------ '5'");
            System.out.println("To get tasks for today enter ------- '6'");
            System.out.println("To exit ---------------------------- '7'");
            try {
                int task = readConsoleNumber();
                switch (task) {
                    case 1:
                        System.out.println("Creating task");
                        System.out.println("Input year...");
                        year = readConsoleNumber();
                        System.out.println("Input month...");
                        month = readConsoleNumber();
                        System.out.println("Input day...");
                        day = readConsoleNumber();
                        calendar.set(year, month, day);
                        date = calendar.getTime();
                        System.out.println(dateFormat.format(date));
                        System.out.println("Input category");
                        String category = readConsole();
                        System.out.println("Input task description");
                        String tasker = readConsole();
                        taskManager.addTask(date, new Task(category, tasker));
                        break;
                    case 2:
                        System.out.println("Input date to remove task");
                        System.out.println("Input year...");
                        year = readConsoleNumber();
                        System.out.println("Input month...");
                        month = readConsoleNumber();
                        System.out.println("Input day...");
                        day = readConsoleNumber();
                        calendar.set(year, month, day);
                        date = calendar.getTime();
                        taskManager.removeTask(date);
                        break;
                    case 3:
                        System.out.println("The categories of tasks are :");
                        for (String cat : taskManager.getCategories()) {
                            System.out.println("[" + cat + "]");
                        }
                        break;
                    case 4:
                        System.out.println("Tasks grouped by categories:");
                        Map<String, List<Task>> tasksByCategories = taskManager.getTasksByCategories();
                        for (Map.Entry<String, List<Task>> entry : tasksByCategories.entrySet()) {
                            System.out.println("By category [" + entry.getKey() + "] :");
                            for (Task tasks : entry.getValue()) {
                                System.out.println("*" + tasks.getCategory() + "=>" + tasks.getDescription() + "*");
                            }
                        }
                        break;
                    case 5:
                        System.out.println("Input the category");
                        String cat = readConsole();
                        for (Task tasks : taskManager.getTasksByCategory(cat)) {
                            System.out.println("*" + tasks.getCategory() + "=>" + tasks.getDescription() + "*");
                        }
                        break;
                    case 6:
                        for (Task tasks : taskManager.getTasksForToday()) {
                            System.out.println("*" + tasks.getCategory() + "=>" + tasks.getDescription() + "*");
                        }
                        break;
                    case 7:
                        cycle = 10;
                        break;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (cycle != 10);
    }
}
