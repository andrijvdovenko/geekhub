package com.geekhub.lessons.lessonFour.taskTwo;

import java.util.*;

public class TaskManagerImpl implements TaskManager {
    private Map<Date, Task> schedule = new TreeMap<>();

    @Override
    public void addTask(Date date, Task task) {
        schedule.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        schedule.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        Set<String> categories = new HashSet<>();
        for (Task task : schedule.values()) {
            categories.add(task.getCategory());
        }
        return categories;
    }

    //For next 3 methods tasks should be sorted by scheduled date
    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> tasksByCategories = new TreeMap<>();
        for (Task task : schedule.values()) {
            String category = task.getCategory();
            if (!tasksByCategories.containsKey(category)) {
                tasksByCategories.put(category, new ArrayList<Task>());
            }
            List<Task> tasks = tasksByCategories.get(category);
            tasks.add(task);
        }
        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        return getTasksByCategories().get(category);
    }

    @Override
    public List<Task> getTasksForToday() {
        Calendar now = Calendar.getInstance();
        List<Task> result = new ArrayList<>();

        for (Date date : schedule.keySet()) {
            Calendar taskCalendar = Calendar.getInstance();
            taskCalendar.setTime(date);

            if (taskCalendar.get(Calendar.YEAR) == now.get(Calendar.YEAR) &&
                    taskCalendar.get(Calendar.DATE) == now.get(Calendar.DATE)) {
                result.add(schedule.get(date));
            }
        }

        return result;
    }
}