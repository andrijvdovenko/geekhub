package com.geekhub.lessons.lessonFour.taskTwo;

import java.util.Date;
import java.util.Map;
import java.util.List;
import java.util.Collection;

public interface TaskManager {

    public void addTask(Date date, Task task);

    public void removeTask(Date date);

    public Collection<String> getCategories();

    public Map<String, List<Task>> getTasksByCategories();

    public List<Task> getTasksByCategory(String category);

    public List<Task> getTasksForToday();
}
