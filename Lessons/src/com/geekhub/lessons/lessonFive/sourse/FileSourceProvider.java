package com.geekhub.lessons.lessonFive.sourse;

import java.io.*;

public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        File file = new File(pathToSource);
        return file.canRead();

    }

    @Override
    public String load(String pathToSource) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(pathToSource));
        StringBuilder s = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            s.append(line);
            s.append("\n");
            }
        br.close();
        return s.toString();

    }
}
