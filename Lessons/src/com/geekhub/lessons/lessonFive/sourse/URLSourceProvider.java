package com.geekhub.lessons.lessonFive.sourse;

import java.io.*;
import java.net.*;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://frpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        try {
            URLConnection connection = new URL(pathToSource).openConnection();
            connection.connect();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public String load(String pathToSource) throws IOException {
        URL url = new URL(pathToSource);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder s = new StringBuilder();
        String line;
        while ((line = br.readLine()) != null) {
            s.append(line);
            s.append("\n");
        }
        br.close();
        return s.toString();
    }
}
