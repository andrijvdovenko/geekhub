package com.geekhub.lessons.lessonFive;

import com.geekhub.lessons.lessonFive.sourse.SourceLoader;
import com.geekhub.lessons.lessonFive.sourse.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) throws IOException {
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());
        System.out.println("Input source to translate or type 'exit' to exit...");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.next();
        while (!"exit".equals(command)) try {
            String source = sourceLoader.loadSource(command);
            String translate = translator.translate(source);
            System.out.println("English text: \n" + source);
            System.out.println("Russian translation: \n" + translate);
            System.out.println("Input source to translate or type 'exit' to exit...");
            command = scanner.next();
        } catch (IOException | NullPointerException e) {
            System.out.println("Error occurred... Please try once more...");
            System.out.println("Input source to translate or type 'exit' to exit...");
            command = scanner.next();
        }
    }
}
